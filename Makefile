MINIPRO=minipro
DEVICE=AT28C256E@PLCC32 
CC=zcc
CFLAGS=+embedded --no-crt -vn -s -m -O3 -startup=1

monitor.ihx: monitor.asm sio.asm
	$(CC) $(CFLAGS) -create-app -Cz--ihex -omonitor.bin monitor.asm sio.asm

flash: monitor.ihx
	$(MINIPRO) -p $(DEVICE) -u -w monitor.ihx

clean:
	- rm -rf *.o *.sym *.map *.bin *.rom *.ihx
