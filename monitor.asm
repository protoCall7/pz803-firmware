        MODULE  monitor

        EXTERN  INIT_SIO
        EXTERN  SIO_PORT_A_WRITE
        EXTERN  SIO_A_TX_BUF_EMPTY_ISR
        EXTERN  SIO_A_EXT_STAT_CHG_ISR
        EXTERN  SIO_A_RX_CHA_AVAIL_ISR
        EXTERN  SIO_A_RX_SPEC_COND_ISR
        EXTERN  SIO_B_TX_BUF_EMPTY_ISR
        EXTERN  SIO_B_EXT_STAT_CHG_ISR
        EXTERN  SIO_B_RX_CHA_AVAIL_ISR
        EXTERN  SIO_B_RX_SPEC_COND_ISR
	EXTERN	CHAR_READY_FLAG
	EXTERN	CHAR_BUFFER

	defc	BS = $08
	defc	CR = $0D
	defc	ESC = $1B
	defc	PROMPT = '\\'

	defc	IN = $8200
	defc	OFFSET = $8300

        ORG     $0000
        SECTION CODE
RST0:
        di                              ; disable interrupts to start off
        jp      START                   ; jump over the reset handlers and IVT

	; rst handlers here

        defs    $0100-$                 ; align the IVT at 0x0100
	INCLUDE "ivt.asm"		; insert interrupt vector table here

START:
        ld      sp, $FFFF               ; set the stack pointer
        im      2                       ; interrupt mode 2
        ld      a, INT_TABLE/256        ; store the MSB of the IVT addr in a
        ld      i, a                    ; load a to i

        call    INIT_SIO                ; set up the SIO

        ei                              ; enable interrupts

;
; Routine to send the startup message to Port A of the SIO
; Must call INIT_SIO first!
;

STARTUP_MESSAGE:
        ld      hl, STARTUP_STR         ; load the address of the startup string to hl

STARTUP_MESSAGE_LOOP:
        ld      a, (hl)                 ; load the first char to a
        or      a                       ; is it a NULL?
        jr      z, NOTCR		; we're done if it is

        ld      b, 0                    ; not a NULL, zero out b
        ld      c, a                    ; store our char in c
        push    bc                      ; put the char on the stack
        call    SIO_PORT_A_WRITE        ; call the write routine
        inc     hl                      ; point to the next char in the string
        jr      STARTUP_MESSAGE_LOOP    ; back to the beginning of the loop

NOTCR:
	cp	BS			; do we have a backspace?
	jr	z, BACKSPACE
	cp	ESC			; do we have an escape?
	jr	z, ESCAPE
	ld	hl,OFFSET		; load the offset address
	inc	(hl)			; increment the offset
	jr	nz, NEXTCHAR		; jump to NEXTCHAR

ESCAPE:
	ld	b, 0			; store 0 in B
	ld	c, PROMPT		; point C at prompt
	push	bc			; push BC to stack
	call	SIO_PORT_A_WRITE	; call the write routine

GETLINE:
	ld	b, 0			; print the prompt
	ld	c, CR
	call	SIO_PORT_A_WRITE

	ld	hl, OFFSET		; initialize the offset to 1
	ld	(hl), 1

BACKSPACE:
	ld	hl, OFFSET		; load the offset to HL
	dec	(hl)			; decrement the offset
	jp	PE, GETLINE		; jump to GETLINE if there was an overflow

NEXTCHAR:
	ld	hl, CHAR_READY_FLAG	; address of the CHAR_READY_FLAG
	bit	7, (hl)			; check if flag is set
	jr	z, NEXTCHAR		; if not set, loop

	ld	hl, CHAR_BUFFER		; store character buffer address in HL
	ld	a, (hl)			; store character to A
	ld	hl, CHAR_READY_FLAG	; store character ready flag address to HL
	res	7, (hl)			; reset the character ready flag

	ld	hl, IN			; address of IN in HL
	ld	de, (OFFSET)		; value at OFFSET in DE
	add	hl, de			; point HL to end of buffer
	ld	a, (hl)			; store char to IN+OFFSET
	ld	c, 0
	ld	b, a			; echo char to screen
	push	bc
	call	SIO_PORT_A_WRITE
	cp	CR			; is it a CR?
	jr	nz, NOTCR

;
; handler for unimplemented ISRs
;

NO_ISR:
        ei                              ; enable interrupts
        reti                            ; go back to what we were doing

        SECTION DATA

STARTUP_STR:
        db      "PZ803 ROMMon v0.1\r\n"
        db      "(c) 2023-2024 - Peter H. Ezetta\r\n", 0
