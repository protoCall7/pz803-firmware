# PZ803 ROM Monitor

This repository contains the source for the ROM monitor for the PZ803 modular
Z80 computer.

- monitor.asm - Main monitor source
- sio.asm - SIO/0 driver source

## Toolchain

The code in this repository can be built with the z88dk toolchain. The Makefile
should function fine with both BSD and GNU make. The `make flash` target is set
up for a Minipro TL866 EEPROM programmer.

## Acknowledgements

- Many thanks to coredump for troubleshooting and debugging assistance
