; SIO Driver for PZ803

        MODULE  sio

        SECTION CODE

        PUBLIC  INIT_SIO
        PUBLIC  SIO_PORT_A_WRITE
        PUBLIC  SIO_A_TX_BUF_EMPTY_ISR
        PUBLIC  SIO_A_EXT_STAT_CHG_ISR
        PUBLIC  SIO_A_RX_CHA_AVAIL_ISR
        PUBLIC  SIO_A_RX_SPEC_COND_ISR
        PUBLIC  SIO_B_TX_BUF_EMPTY_ISR
        PUBLIC  SIO_B_EXT_STAT_CHG_ISR
        PUBLIC  SIO_B_RX_CHA_AVAIL_ISR
        PUBLIC  SIO_B_RX_SPEC_COND_ISR
	PUBLIC	CHAR_READY_FLAG
	PUBLIC	CHAR_BUFFER

        defc    SIO_DATA_A=$00          ; address of SIO A/D
        defc    SIO_DATA_B=$01          ; address of SIO B/D
        defc    SIO_CTRL_A=$02          ; address of SIO A/C
        defc    SIO_CTRL_B=$03          ; address of SIO B/C
        defc    INT_VECTOR=$00          ; interrupt vector register value
        defc    CONF_STR_N=4            ; number of config strings to send
	defc	CHAR_READY_FLAG=$8000
	defc	CHAR_BUFFER=$8100

; Initialize SIO
INIT_SIO:
        push    af                      ; save the af register to the stack
        push    bc                      ; save the bc register to the stack
        push    hl                      ; save the hl register to the stack

	;
	; To receive or transmit data in the Asynchronous mode, the Z80 SIO must
	; be initialized with the following parameters: character length, clock rate,
	; number of stop bits, even or odd parity, interrupt mode, and receiver or
	; transmitter enable.
	;

        call    SIO_A_RESET             ; reset SIO Channel A
        call    SIO_B_RESET             ; reset SIO Channel B

        ld      a, CONF_STR_N           ; load the number of config strings to a
        ld      hl, SIO_INIT_STR        ; port first

CONFIG_LOOP:
        ld      c, (hl)                 ; address of SIO port in c
        inc     hl                      ; point at the length
        ld      b, (hl)                 ; length of byte string
        inc     hl                      ; point at the bytestring
        otir                            ; send bytes

        dec     a                       ; decrement our counter
        or      a                       ; is a 0 now?
        jr      nz, CONFIG_LOOP         ; jump back to the loop if not

        pop     hl                      ; restore hl
        pop     bc                      ; restore bc
        pop     af                      ; restore af

        ret                             ; return

;
; Send SIO channel A the reset command
;

SIO_A_RESET:
        push    af                      ; preserve af register
        ld      a, %00011000            ; store channel reset command in a
        out     (SIO_CTRL_A), a         ; send it to channel a
        pop     af                      ; restore af register
        ret                             ; return

;
; Send SIO channel B the reset command
;

SIO_B_RESET:
        push    af                      ; preserve af register
        ld      a, %00011000            ; store channel reset command in a
        out     (SIO_CTRL_B), a         ; send it to channel b
        pop     af                      ; restore af register
        ret                             ; return

;
; Write a character from the stack to the console
;

SIO_PORT_A_WRITE:
        pop     bc                      ; Return Address is first off the stack
        pop     de                      ; Character stored in e
        push    bc                      ; Restore the return address to the stack for ret
        push    af                      ; save af register

        ld      a, e                    ; move the character to A
        out     (SIO_DATA_A), a         ; send the character to the SIO

        call    SIO_A_TX_WAIT           ; block until the character is sent

        pop     af                      ; restore af register

        ret

;
; loop until SIO channel A's tx buffer is empty
;
SIO_A_TX_WAIT:
        push    af                      ; preserve af

TX_BUSY_LOOP_A:
        ld      a, %00000000            ; select RR0
        out     (SIO_CTRL_A), a         ; send the pointer
        in      a, (SIO_CTRL_A)         ; read byte from RR0
        bit     2, a                    ; test for the tx buffer being empty
        jr      z, TX_BUSY_LOOP_A       ; loop until buffer becomes empty

        pop     af                      ; restore af
        ret

;
; loop until SIO channel B's tx buffer is empty
;
SIO_B_TX_WAIT:
        push    af                      ; preserve af

TX_BUSY_LOOP_B:
        ld      a, %00000000            ; select RR0
        out     (SIO_CTRL_B), a         ; send the pointer
        in      a, (SIO_CTRL_B)         ; read byte from RR0
        bit     2, a                    ; test for the tx buffer being empty
        jr      z, TX_BUSY_LOOP_B       ; loop until buffer becomes empty

        pop     af                      ; restore af
        ret

;
; enable the RTS line on SIO A
;

SIO_PORT_A_RTS_ON:
        push    af                      ; preserve the af register

        ld      a, %00000101            ; point to WR5
        out     (SIO_CTRL_A), a         ; send pointer to channel a
        ld      a, %11101010            ; enable the RTS bit
        out     (SIO_CTRL_A), a         ; send the updated register

        pop     af                      ; restore af register
        ret                             ; return

;
; disable the RTS line on SIO A
;

SIO_PORT_A_RTS_OFF:
        push    af                      ; preserve the af register

        ld      a, %00000101            ; point to WR5
        out     (SIO_CTRL_A), a         ; send the pointer to channel a
        ld      a, %11101000            ; disable the RTS bit
        out     (SIO_CTRL_A), a         ; send the updated register

        pop     af                      ; restore af register
        ret                             ; return

;
; ISR for Port A Receive Character Available
;

SIO_A_RX_CHA_AVAIL_ISR:
        ex      af, af'                 ; swap to alternate af register
        exx                             ; swap the rest of the registers to alternates

        in      a, (SIO_DATA_A)         ; read a char from the SIO

	ld	hl, CHAR_BUFFER		; store the buffer address in HL
	ld	(hl), a			; load char to buffer

	ld	hl, CHAR_READY_FLAG	; store the flag address in DE
	set	7, (HL)			; set CHAR_READY_FLAG

        exx                             ; swap back to original registers
        ex      af, af'                 ; swap af back too

        ei                              ; re-enable interrupts
        reti                            ; return from the interrupt

SIO_B_RX_CHA_AVAIL_ISR:
        ex      af, af'                 ; swap to alternate af register
        exx                             ; swap the rest of the registers to alternates

        in      a, (SIO_DATA_B)         ; read a char from the SIO
        out     (SIO_DATA_B), a         ; echo it back out
        call    SIO_B_TX_WAIT           ; wait for TX buffer to clear

        exx                             ; swap back to original registers
        ex      af, af'                 ; swap af back too

        ei                              ; re-enable interrupts
        reti                            ; return from the interrupt

SIO_A_RX_SPEC_COND_ISR:
        ex      af, af'
        exx

CLEAR_A_RX_BUFFER:
        in      a, (SIO_DATA_A)         ; read in a byte from the SIO rx buffer
        ld      a, %00000000            ; pointer to RR0
        out     (SIO_CTRL_A), a         ; select RR0
        in      a, (SIO_CTRL_A)         ; read RR0 to a
        bit     0, a                    ; check if there's still a char in the RX buffer
        jr      nz, CLEAR_A_RX_BUFFER   ; to read another char if the buffer isn't empty

        ld      a, %00110000            ; error reset command to a
        out     (SIO_CTRL_A), a         ; send command to SIO Ch A

        ld      a, '!'                  ; debug output char
        out     (SIO_DATA_A), a         ; send the debug output
        call    SIO_A_TX_WAIT           ; wait for it to send

        exx                             ; swap registers back
        ex      af, af                  ; swap af registers back

        ei                              ; enable interrupts
        reti                            ; return from ISR

; unimplemented ISRs

SIO_A_TX_BUF_EMPTY_ISR:
SIO_A_EXT_STAT_CHG_ISR:
SIO_B_TX_BUF_EMPTY_ISR:
SIO_B_EXT_STAT_CHG_ISR:
SIO_B_RX_SPEC_COND_ISR:
        ei
        reti

;
; DATA Section
;

        SECTION DATA

SIO_INIT_STR:
        defb    SIO_CTRL_B
        defb    2                       ; send 2 commands to B
        defb    %00000010               ; select ch b WR2
        defb    %00000000               ; 0x00 ch b Int Vector

        defb    SIO_CTRL_A
        defb    6                       ; send 6 commands to A
        defb    %00000100               ; select WR4
        defb    %01000100               ; 16x clock, 1 stop bit, no parity
        defb    %00000011               ; select WR3
        defb    %11000001               ; 8 bits, auto enables off, enable rx
        defb    %00000101               ; select WR5
        defb    %11101000               ; DTR active, 8 bits, enable tx

        defb    SIO_CTRL_B
        defb    2
        defb    %00000001               ; pointer to WR1
        defb    %00000100               ; status affects vector

        defb    SIO_CTRL_A
        defb    2
        defb    %00000001
        defb    %00011000               ; status affects vector

; End of device driver

